# GIPHY

## Requirments
```
Symfony client
PHP 7.4.x
MySQL 8
```

## Server
```bin
symfony serve
```

## Webpack for production
```bin
yarn dev
```
ou
```bin
npm run dev
```

## Webpack for developpment
```bin
yarn watch
```
ou
```bin
npm run watch
```
