<?php

namespace App\Controller\Profile;

use App\Entity\Gif;
use App\Form\GifType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\ByteString;

/**
 * @Route("/profile", name="profile")
 */
class ProfileController extends AbstractController
{
    private RequestStack $requestStack;
    private Request $request;
    private EntityManagerInterface $entityManager;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name=".index")
     */
    public function index(): Response
    {
        return $this->render('profile/index.html.twig');
    }

    /**
     * @Route("/add", name=".add")
     */
    public function add(): Response
    {
        $gif = new Gif();
        $form = $this->createForm(GifType::class, $gif);
        $form->handleRequest($this->request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $gif->setUser($this->getUser());

            $imageName = ByteString::fromRandom(32)->lower();
            $imageExtension = $gif->getSource()->guessExtension();
            $gif->getSource()->move('img', "$imageName.$imageExtension");

            $gif
                ->setSlug("$imageName")
                ->setSource("$imageName.$imageExtension")
            ;

            $this->entityManager->persist($gif);
            $this->entityManager->flush();

            return $this->redirectToRoute('profile.index');
        }

        return $this->render('profile/add.html.twig', [
            'addForm' => $form->createView()
        ]);
    }
}
