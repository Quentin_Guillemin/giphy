<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/category", name="category")
 */
class CategoryController extends AbstractController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/{categorySlug}", name=".index")
     */
    public function index(string $categorySlug): Response
    {
        $category = $this->categoryRepository->findOneBy(['slug' => $categorySlug]);

        $subCategories = $this->categoryRepository->findSubCategoriesByCategorySlug($categorySlug);

        return $this->render('category/index.html.twig', [
            'category' => $category,
            'sub_categories' => $subCategories
        ]);
    }

    /**
     * @Route("/{categorySlug}/{subCategorySlug}", name=".sub_category_index")
     */
    public function subCategoryIndex(string $categorySlug, string $subCategorySlug): Response
    {
        $subCategory = $this->categoryRepository->findOneBy(['slug' => $subCategorySlug]);

        return $this->render('category/subcategory.html.twig', [
            'sub_category' => $subCategory
        ]);
    }
}
