<?php

namespace App\Controller;

use App\Repository\GifRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/gif", name="gif")
 */
class GifController extends AbstractController
{
    private GifRepository $gifRepository;

    public function __construct(GifRepository $gifRepository)
    {
        $this->gifRepository = $gifRepository;
    }

    /**
     * @Route("/{gifSlug}", name=".index")
     */
    public function index(string $gifSlug): Response
    {
        $gif = $this->gifRepository->findOneBy(['slug' => $gifSlug]);

        return $this->render('gif/index.html.twig', [
            'gif' => $gif,
        ]);
    }
}
