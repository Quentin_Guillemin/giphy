<?php

namespace App\Controller;

use App\Repository\GifRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/", name="homepage")
 */
class HomepageController extends AbstractController
{
    private GifRepository $gifRepository;

    public function __construct(GifRepository $gifRepository)
    {
        $this->gifRepository = $gifRepository;
    }

    /**
     * @Route("/", name=".index")
     */
    public function index(): Response
    {
        $gifs = $this->gifRepository->findAll();

        return $this->render('homepage/index.html.twig', [
            'gifs' => $gifs
        ]);
    }
}
