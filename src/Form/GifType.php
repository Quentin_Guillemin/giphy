<?php

namespace App\Form;

use App\Entity\Gif;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class GifType extends AbstractType
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source', FileType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'This field is required'
                    ]),
                    new Image([
                        'mimeTypes' => ['image/gif', 'image/webp'],
                        'mimeTypesMessage' => 'This type of file is not allowed'
                    ])
                ],
                'help' => 'Only .gif and .webp are allowed'
            ])
            // ->add('slug')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'query_builder' => $this->categoryRepository->findSubCategories(),
                'group_by' => 'parent.name',
                'placeholder' => 'Choose a category',
                'constraints' => [
                    new NotBlank([
                        'message' => 'This field is required'
                    ])
                ]
            ])
            // ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gif::class,
        ]);
    }
}
